import React from 'react';
import { useTranslation } from 'react-i18next';
import Page from 'shopeefrontend/components/Page';
import RegisterView from 'shopeefrontend/features/authentication/register/RegisterView';

function Register() {
  const { t } = useTranslation();

  return (
    <Page title={t('Đăng ký')}>
      <RegisterView />
    </Page>
  );
}

export default Register;
