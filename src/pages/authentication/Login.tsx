import React from 'react';
import { useTranslation } from 'react-i18next';
import LoginView from 'shopeefrontend/features/authentication/login/LoginView';
import Page from 'shopeefrontend/components/Page';

function Login() {
  const { t } = useTranslation();
  return (
    <Page title={t('Đăng nhập')}>
      <LoginView />
    </Page>
  );
}

export default Login;
