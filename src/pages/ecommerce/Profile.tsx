import React from 'react';
import Page from 'shopeefrontend/components/Page';
import WithAuthentication from 'shopeefrontend/components/HOC/WithAuthentication';

function Profile() {
  return (
    <WithAuthentication>
      <Page title='Profile'>Profile authen page</Page>
    </WithAuthentication>
  );
}

export default Profile;
