import { RegisterFormProps } from 'shopeefrontend/features/authentication/register/types';
import http from 'shopeefrontend/utils/http';
import { LoginFormProps, LoginResponse } from 'shopeefrontend/features/authentication/login/types';
import { ResponseApi } from 'shopeefrontend/types/api.types';
import { User } from 'shopeefrontend/types/user.types';

const register = (data: RegisterFormProps) => {
  return http.post('/register', data);
};
const login = (data: LoginFormProps) => {
  return http.post<ResponseApi<LoginResponse>, LoginFormProps>('/login', data);
};

const getProfileInfo = () => {
  return http.get<ResponseApi<User>>('/me').then((res) => res.data);
};

export const authServices = {
  register,
  login,
  getProfileInfo,
};
