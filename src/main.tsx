/* eslint-disable @typescript-eslint/no-empty-function */
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import { HelmetProvider } from 'react-helmet-async';

import 'shopeefrontend/locales/i18n';
import 'shopeefrontend/styles/index.css';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { INITIAL_REACT_QUERY_CONFIGS } from 'shopeefrontend/utils/configs';
import { currentEnvMode } from 'shopeefrontend/utils/constants';
import { RecoilRoot } from 'recoil';

const queryClient = new QueryClient(INITIAL_REACT_QUERY_CONFIGS);

if (currentEnvMode === 'production') {
  console.log = () => {};
  console.error = () => {};
  console.debug = () => {};
}

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <HelmetProvider>
    <RecoilRoot>
      <BrowserRouter>
        <QueryClientProvider client={queryClient}>
          <App />
          <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
      </BrowserRouter>
    </RecoilRoot>
  </HelmetProvider>,
);
