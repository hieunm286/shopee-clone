import { QueryClientConfig } from '@tanstack/react-query';

export const INITIAL_REACT_QUERY_CONFIGS: QueryClientConfig = {
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      keepPreviousData: true,
    },
  },
};
