import { ErrorUtilsHandleErrorOptions } from 'shopeefrontend/types/error.types';
import { ResponseError, ResponseErrorData } from 'shopeefrontend/types/api.types';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { isNotFoundError, isUnauthorizedError, isUnprocessableEntityError } from 'shopeefrontend/utils/apis';

export class ErrorApi<T = ResponseErrorData> {
  readonly status: number;
  readonly data: ResponseError<T>;
  readonly config: AxiosRequestConfig;

  constructor(response: AxiosResponse<ResponseError<T>>) {
    this.status = response.status;
    this.data = response.data;
    this.config = response.config;
  }
}

export class ErrorUtils {
  static handleError<T>(err: unknown, options?: ErrorUtilsHandleErrorOptions<T>) {
    const {
      fn,
      isErrorInstanceFn,
      isNotErrorInstanceFn,
      isUnprocessableEntityErrorFn,
      isNotFoundErrorFn,
      isUnauthorizedErrorFn,
    } = options ?? {};
    console.log(err);
    if (err instanceof ErrorApi) {
      // do something
      isErrorInstanceFn && isErrorInstanceFn(err);
      if (isUnprocessableEntityError(err)) {
        isUnprocessableEntityErrorFn && isUnprocessableEntityErrorFn(err);
      }
      if (isNotFoundError(err)) {
        isNotFoundErrorFn && isNotFoundErrorFn(err);
      }
      if (isUnauthorizedError(err)) {
        isUnauthorizedErrorFn && isUnauthorizedErrorFn(err);
      }
    } else {
      isNotErrorInstanceFn && isNotErrorInstanceFn();
    }
    fn && fn();
  }
}
