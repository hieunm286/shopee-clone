export const saveToLocalStorage = (key: string, data: any) => {
  localStorage.setItem(key, JSON.stringify(data));
};

export const getDataFromLocalStorage = (key: string) => {
  const data = localStorage.getItem(key);
  if (!data) return;
  return JSON.parse(data);
};

export const getStringFromLocalStorage = (key: string) => {
  return localStorage.getItem(key);
};
