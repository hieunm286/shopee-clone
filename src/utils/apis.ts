import { ErrorApi } from 'shopeefrontend/utils/error';
import { HttpStatusCode } from 'axios';

// Check if error status code is 401
export const isUnauthorizedError = (error: ErrorApi) => error.status === HttpStatusCode.Unauthorized;

// Check if error status code is 422
export const isUnprocessableEntityError = (error: ErrorApi) => error.status === HttpStatusCode.UnprocessableEntity;

// Check if error status code is 404
export const isNotFoundError = (error: ErrorApi) => error.status === HttpStatusCode.NotFound;
