import http from 'shopeefrontend/utils/http';

export const setSession = (accessToken?: string) => {
  if (accessToken) {
    localStorage.setItem('accessToken', accessToken);
    http.setAuthorization(accessToken);
  } else {
    localStorage.removeItem('accessToken');
    http.clearAuthorization();
  }
};
