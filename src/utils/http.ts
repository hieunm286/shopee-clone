import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { apiEndpoint } from 'shopeefrontend/utils/constants';
import { ResponseError } from 'shopeefrontend/types/api.types';
import { ErrorApi } from 'shopeefrontend/utils/error';

class Http {
  protected readonly instance: AxiosInstance;

  constructor(baseURL?: string) {
    this.instance = axios.create({
      baseURL,
    });

    this.configResponseInterceptor();
  }

  private configResponseInterceptor() {
    this.instance.interceptors.response.use(this.handleResponse, this.handleError);
  }

  protected handleResponse({ data }: AxiosResponse) {
    return Promise.resolve(data);
  }

  protected handleError(error: AxiosError<ResponseError>) {
    if (!error.response) return Promise.reject(error);
    return Promise.reject(new ErrorApi(error.response));
  }

  setAuthorization(accessToken: string) {
    this.instance.defaults.headers.common.authorization = accessToken;
  }

  clearAuthorization() {
    delete this.instance.defaults.headers.common.authorization;
  }

  get<T, P = {}>(url: string, params?: P, config?: Omit<AxiosRequestConfig, 'params'>): Promise<T> {
    return this.instance({
      method: 'GET',
      url,
      params,
      ...config,
    }) as Promise<T>;
  }

  post<T, D>(url: string, data?: D, config?: Omit<AxiosRequestConfig, 'data'>): Promise<T> {
    return this.instance({
      method: 'POST',
      url,
      data,
      ...config,
    }) as Promise<T>;
  }

  put<T>(url: string, data?: T, config?: Omit<AxiosRequestConfig, 'data'>): Promise<T> {
    return this.instance({
      method: 'PUT',
      url,
      data,
      ...config,
    }) as Promise<T>;
  }

  delete<T>(url: string, config?: Omit<AxiosRequestConfig, 'data'>): Promise<T> {
    return this.instance({
      method: 'DELETE',
      url,
      ...config,
    }) as Promise<T>;
  }
}

const http = new Http(apiEndpoint);

export default http;
