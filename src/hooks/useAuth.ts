import { User } from 'shopeefrontend/types/user.types';
import { useSetRecoilState } from 'recoil';
import { authenticationAtom, userAtom } from 'shopeefrontend/recoil/atom/user.atom';
import { authServices } from 'shopeefrontend/apis';
import { ErrorUtils } from 'shopeefrontend/utils/error';
import { setSession } from 'shopeefrontend/utils/sessions';
import { useNavigate } from 'react-router-dom';
import appRoutes from 'shopeefrontend/routes/paths';

export default function useAuth() {
  const setUser = useSetRecoilState(userAtom);
  const setAuthentication = useSetRecoilState(authenticationAtom);
  const navigate = useNavigate();

  const updateUserInfoAsync = async (
    options: { redirectIfUnauthorized: boolean } = { redirectIfUnauthorized: false },
  ) => {
    try {
      const user = await authServices.getProfileInfo();
      if (user) {
        setUser(user);
        setAuthentication((prevState) => ({
          ...prevState,
          isInitialized: true,
        }));
      }
    } catch (err) {
      ErrorUtils.handleError(err, {
        isUnprocessableEntityErrorFn: (error) => {
          console.log(error);
        },
        isNotFoundErrorFn: () => {
          alert('API Not Found');
        },
        isUnauthorizedErrorFn: (err) => {
          console.log(err);
          setSession(undefined);
          setAuthentication({
            accessToken: undefined,
            isInitialized: true,
          });
          setUser(undefined);
          if (options.redirectIfUnauthorized) {
            console.log('redirect');
            navigate(appRoutes.login.fullPath);
          }
        },
      });
    }
  };

  const updateUserInfo = (user?: User) => {
    if (!user) return;
    setUser(user);
  };

  return {
    updateUserInfo,
    updateUserInfoAsync,
  };
}
