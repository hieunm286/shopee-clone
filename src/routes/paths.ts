export function path(root: string, subLink: string): string {
  return `${root}${subLink}`;
}

const AUTH_PREFIX = '/auth';

const ECOMMERCE_PREFIX = '/';

const authenticationRoutes = {
  register: {
    path: 'register',
    fullPath: path(AUTH_PREFIX, '/register'),
  },
  login: {
    path: 'login',
    fullPath: path(AUTH_PREFIX, '/login'),
  },
};

const ecommerceRoutes = {
  products: {
    path: '',
    fullPath: path(ECOMMERCE_PREFIX, ''),
  },
  profile: {
    path: 'profile',
    fullPath: path(ECOMMERCE_PREFIX, '/profile'),
  },
};

const appRoutes = {
  ...authenticationRoutes,
  ...ecommerceRoutes,
};

export default appRoutes;
