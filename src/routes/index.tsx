import React, { lazy, Suspense } from 'react';
import { Navigate, useRoutes } from 'react-router-dom';
import AuthenticationLayout from 'shopeefrontend/layouts/authentication/AuthenticationLayout';
import EcommerceLayout from 'shopeefrontend/layouts/ecommerce/EcommerceLayout';
import appRoutes from 'shopeefrontend/routes/paths';

const Loadable =
  <C extends React.ComponentType<any>, P extends React.ComponentProps<C>>(Component: C) =>
  (props: P) => {
    return (
      <Suspense fallback={<></>}>
        <Component {...props} />
      </Suspense>
    );
  };

export default function Routes() {
  return useRoutes([
    {
      path: 'auth',
      element: <AuthenticationLayout />,
      children: [
        {
          path: appRoutes.login.path,
          element: <Login />,
        },
        {
          path: appRoutes.register.path,
          element: <Register />,
        },
        {
          index: true,
          element: <Navigate to={appRoutes.login.path} />,
        },
      ],
    },
    {
      path: '/',
      element: <EcommerceLayout />,
      children: [
        {
          index: true,
          element: <Products />,
        },
        {
          path: appRoutes.profile.path,
          element: <Profile />,
        },
      ],
    },
  ]);
}

const Products = Loadable(lazy(() => import('shopeefrontend/pages/ecommerce/Products')));
const Profile = Loadable(lazy(() => import('shopeefrontend/pages/ecommerce/Profile')));
const Login = Loadable(lazy(() => import('shopeefrontend/pages/authentication/Login')));
const Register = Loadable(lazy(() => import('shopeefrontend/pages/authentication/Register')));
