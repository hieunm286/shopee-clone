import React from 'react';
import { useRecoilValue } from 'recoil';
import { authenticatedSelector } from 'shopeefrontend/recoil/atom/user.atom';
import { Navigate } from 'react-router-dom';
import appRoutes from 'shopeefrontend/routes/paths';

function WithAuthentication({ children }: React.PropsWithChildren) {
  const isAuthenticated = useRecoilValue(authenticatedSelector);

  if (!isAuthenticated) {
    return <Navigate to={appRoutes.login.fullPath} />;
  }

  return <>{children}</>;
}

export default WithAuthentication;
