import { Helmet } from 'react-helmet-async';
import { ForwardedRef, forwardRef, PropsWithChildren } from 'react';

interface PageProps {
  title?: string;
}

function Page({ children, title = '', ...other }: PropsWithChildren<PageProps>, ref: ForwardedRef<HTMLDivElement>) {
  return (
    <div ref={ref} {...other}>
      <Helmet>
        <title>{title}</title>
      </Helmet>
      {children}
    </div>
  );
}

export default forwardRef(Page);
