import React, { ForwardedRef } from 'react';
import { ButtonProps } from './types';
import classNames from 'shopeefrontend/utils/classNames';

function Button(
  { children, className = '', rootClassName = '', fullWidth }: ButtonProps,
  ref: ForwardedRef<HTMLButtonElement>,
) {
  return (
    <div className={classNames('flex gap-2 items-center', rootClassName)}>
      <button
        ref={ref}
        className={classNames(
          'bg-primary-main hover:bg-primary-lighter rounded px-4 py-2 text-white text-sm',
          fullWidth ? 'w-full' : '',
          className,
        )}
      >
        {children}
      </button>
    </div>
  );
}

export default React.forwardRef(Button);
