import { ComponentPropsWithoutRef, ReactNode } from 'react';

export interface ButtonProps extends ComponentPropsWithoutRef<'button'> {
  className?: string;
  rootClassName?: string;
  children?: string | ReactNode | JSX.Element;
  fullWidth?: boolean;
}
