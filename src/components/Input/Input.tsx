import React, { ForwardedRef } from 'react';
import { InputProps } from './types';
import classNames from 'shopeefrontend/utils/classNames';

const errorClassName = 'border-red-600 text-red-600';

function Input(
  { className, inputClassName = 'focus:text-black', error, helperText, focusedOnMount, ...props }: InputProps,
  ref: ForwardedRef<HTMLInputElement>,
) {
  return (
    <div
      className={className}
      ref={(ref) => {
        if (ref && ref.children[0] && focusedOnMount) {
          (ref.children[0] as HTMLInputElement).focus();
        }
      }}
    >
      <input
        {...props}
        ref={ref}
        className={classNames(
          'p-3 w-full outline-none border border-gray-300 focus:border-gray-500 rounded-sm focus:shadow-sm',
          error ? errorClassName : '',
          inputClassName,
        )}
      />
      <div className={classNames('min-h-1.25 text-sm', error ? errorClassName : 'text-gray-400')}>{helperText}</div>
    </div>
  );
}

export default React.forwardRef(Input);
