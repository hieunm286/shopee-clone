import { ComponentPropsWithoutRef } from 'react';

export interface InputProps extends ComponentPropsWithoutRef<'input'> {
  className?: string;
  inputClassName?: string;
  error?: boolean;
  helperText?: string;
  focusedOnMount?: boolean;
}
