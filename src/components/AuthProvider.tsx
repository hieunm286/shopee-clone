import React, { useEffect } from 'react';
import { getStringFromLocalStorage } from 'shopeefrontend/utils/localStorage';
import { setSession } from 'shopeefrontend/utils/sessions';
import useAuth from 'shopeefrontend/hooks/useAuth';

function AuthProvider({ children }: React.PropsWithChildren) {
  const { updateUserInfoAsync } = useAuth();

  useEffect(() => {
    const fetchingUserProfile = async () => {
      const accessToken = getStringFromLocalStorage('accessToken');
      if (accessToken) {
        setSession(accessToken);
        await updateUserInfoAsync({ redirectIfUnauthorized: true });
      }
    };

    void fetchingUserProfile();
  }, []);

  return <>{children}</>;
}

export default AuthProvider;
