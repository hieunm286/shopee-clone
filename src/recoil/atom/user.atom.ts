import { atom, selector } from 'recoil';
import { User } from 'shopeefrontend/types/user.types';
import { getStringFromLocalStorage } from 'shopeefrontend/utils/localStorage';

export const userAtom = atom<User | undefined>({
  key: 'userAtom',
  default: undefined,
});

export const authenticationAtom = atom<{ accessToken?: string | null | undefined; isInitialized: boolean }>({
  key: 'authenticationAtom',
  default: {
    accessToken: getStringFromLocalStorage('accessToken'),
    isInitialized: false,
  },
});

export const authenticatedSelector = selector({
  key: 'authenticatedSelector',
  get: ({ get }) => {
    const user = get(userAtom);
    const { accessToken, isInitialized } = get(authenticationAtom);
    return !(!accessToken || (accessToken && isInitialized && !user));
  },
});
