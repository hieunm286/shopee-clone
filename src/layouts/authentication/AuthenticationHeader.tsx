import React from 'react';
import { StaticIcons } from 'shopeefrontend/assets/StaticIcons';
import { useTranslation } from 'react-i18next';

function AuthenticationHeader() {
  const { t } = useTranslation();
  return (
    <nav className='h-21 flex justify-between items-center container mx-auto'>
      <div className='flex items-end gap-4'>
        <StaticIcons.Shopee />
        <p className='text-2xl'>{t('welcome')}</p>
      </div>
      <p className='textSm text-primary-main cursor-pointer'>{t('Bạn cần giúp đỡ?')}</p>
    </nav>
  );
}

export default AuthenticationHeader;
