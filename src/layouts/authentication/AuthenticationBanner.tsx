import React from 'react';
import { Outlet } from 'react-router-dom';

function AuthenticationBanner() {
  return (
    <div className='bg-primary-main'>
      <div className='bg-authenticationBanner bg-no-repeat h-150 flex justify-end max-w-260 mx-auto'>
        <div className='h-full flex items-center'>
          <Outlet />
        </div>
      </div>
    </div>
  );
}

export default AuthenticationBanner;
