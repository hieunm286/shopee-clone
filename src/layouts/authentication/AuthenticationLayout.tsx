import React from 'react';
import AuthenticationHeader from 'shopeefrontend/layouts/authentication/AuthenticationHeader';
import AuthenticationBanner from 'shopeefrontend/layouts/authentication/AuthenticationBanner';
import AuthenticationFooter from 'shopeefrontend/layouts/authentication/AuthenticationFooter';
import { useRecoilValue } from 'recoil';
import { authenticationAtom } from 'shopeefrontend/recoil/atom/user.atom';
import { Navigate } from 'react-router-dom';
import appRoutes from 'shopeefrontend/routes/paths';

function AuthenticationLayout() {
  const { accessToken } = useRecoilValue(authenticationAtom);

  if (accessToken) {
    return <Navigate to={appRoutes.products.fullPath} replace />;
  }

  return (
    <>
      <AuthenticationHeader />
      <AuthenticationBanner />
      <AuthenticationFooter />
    </>
  );
}

export default AuthenticationLayout;
