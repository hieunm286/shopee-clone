import React from 'react';
import { Outlet } from 'react-router-dom';
import EcommerceHeader from 'shopeefrontend/layouts/ecommerce/EcommerceHeader';

function EcommerceLayout() {
  return (
    <>
      <EcommerceHeader />
      EcommerceLayout - Test 29/1
      <Outlet />
    </>
  );
}

export default EcommerceLayout;
