export enum UserRoles {
    User = 'User',
    Admin = 'Admin'
}

export interface User {
    _id: string;
    roles: UserRoles[];
    email: string;
    name: string;
    date_of_birth?: null;
    address: string;
    phone: string;
    createdAt: string;
    updatedAt: string;
    __v: number;
}
