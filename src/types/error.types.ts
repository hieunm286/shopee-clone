import {ErrorApi} from "shopeefrontend/utils/error";

export interface ErrorUtilsHandleErrorOptions<T> {
    isErrorInstanceFn?: (err: ErrorApi<T>) => void;
    isNotErrorInstanceFn?: () => void;
    isUnprocessableEntityErrorFn?: (err: ErrorApi<T>) => void;
    isNotFoundErrorFn?: (err: ErrorApi<T>) => void;
    isUnauthorizedErrorFn?: (err: ErrorApi<T>) => void;
    fn?: () => void;
}