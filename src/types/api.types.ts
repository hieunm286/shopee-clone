export interface ResponseApi<T> {
    message: string;
    data?: T
}

export type ResponseErrorData = {
    [X: string]: string
}

export type ResponseError<T = ResponseErrorData> = ResponseApi<T>