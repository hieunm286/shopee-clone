import { useMutation } from '@tanstack/react-query';
import { authServices } from 'shopeefrontend/apis';

const register = () => useMutation(authServices.register);

export const registerQueries = {
  register,
};
