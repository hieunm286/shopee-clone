import { defaultRegisterFormValues } from 'shopeefrontend/features/authentication/register/constants';

export type RegisterFormProps = typeof defaultRegisterFormValues;
