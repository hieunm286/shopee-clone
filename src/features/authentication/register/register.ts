import { useForm } from 'react-hook-form';
import { defaultRegisterFormValues } from 'shopeefrontend/features/authentication/register/constants';
import { RegisterFormProps } from 'shopeefrontend/features/authentication/register/types';
import { registerQueries } from 'shopeefrontend/features/authentication/register/queries';
import { ErrorUtils } from 'shopeefrontend/utils/error';
import { useNavigate } from 'react-router-dom';

export function registerHandler() {
  const navigate = useNavigate();
  const { handleSubmit, ...form } = useForm<RegisterFormProps>({
    defaultValues: defaultRegisterFormValues,
  });
  const { mutateAsync } = registerQueries.register();

  const onRegister = handleSubmit(async (data) => {
    try {
      const result = await mutateAsync(data);
      console.log(result);
    } catch (e) {
      ErrorUtils.handleError<RegisterFormProps>(e, {
        isUnprocessableEntityErrorFn: (error) => {
          const { email, password } = error.data.data ?? {};
          if (email) {
            form.setError('email', { message: email });
          }
          if (password) {
            form.setError('password', { message: password });
          }
        },
        isNotFoundErrorFn: () => {
          alert('API Not Found');
        },
      });
    }
  });

  return {
    form,
    onRegister,
  };
}
