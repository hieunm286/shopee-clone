import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Input } from 'shopeefrontend/components';
import { Link } from 'react-router-dom';
import appRoutes from 'shopeefrontend/routes/paths';
import { registerHandler } from 'shopeefrontend/features/authentication/register/register';

function RegisterView() {
  const { t } = useTranslation();
  const {
    form: {
      formState: { errors },
      register,
    },
    onRegister,
  } = registerHandler();

  return (
    <div className='bg-white p-3 w-25 px-7 py-6 rounded'>
      <p className='text-xl'>{t('Đăng nhập')}</p>
      <form onSubmit={onRegister}>
        <Input
          type='email'
          className='mt-8'
          error={!!errors.email}
          helperText={errors.email?.message}
          placeholder={t('Nhập email')}
          focusedOnMount
          {...register('email')}
        />
        <Input
          type='password'
          error={!!errors.password}
          helperText={errors.password?.message}
          placeholder={t('Nhập mật khẩu')}
          {...register('password')}
        />
        <Input
          type='confirmPassword'
          error={!!errors.confirmPassword}
          helperText={errors.confirmPassword?.message}
          placeholder={t('Nhập lại mật khẩu')}
          {...register('confirmPassword')}
        />
        <Button type='submit' fullWidth>
          {t('Đăng ký')}
        </Button>
      </form>
      <hr className='my-4' />
      <div className='text-center'>
        <span className='text-gray-400 text-sm'>{t('Bạn đã có tài khoản Shopee?')}</span>{' '}
        <Link to={appRoutes.login.fullPath}>
          <span className='text-sm text-primary-main'>{t('Đăng nhập')}</span>
        </Link>
      </div>
    </div>
  );
}

export default RegisterView;
