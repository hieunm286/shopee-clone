import { useForm } from 'react-hook-form';
import { defaultLoginFormValues } from 'shopeefrontend/features/authentication/login/constants';
import { loginQueries } from 'shopeefrontend/features/authentication/login/queries';
import { ErrorUtils } from 'shopeefrontend/utils/error';
import { LoginFormProps } from 'shopeefrontend/features/authentication/login/types';
import useAuth from 'shopeefrontend/hooks/useAuth';
import { setSession } from 'shopeefrontend/utils/sessions';
import { useNavigate } from 'react-router-dom';
import appRoutes from 'shopeefrontend/routes/paths';

export function loginHandler() {
  const { updateUserInfo } = useAuth();
  const navigate = useNavigate();
  const loginMutation = loginQueries.login();
  const { handleSubmit, ...form } = useForm<typeof defaultLoginFormValues>({ defaultValues: defaultLoginFormValues });

  const onLogin = handleSubmit(async (data) => {
    try {
      const result = await loginMutation.mutateAsync(data);
      if (result && result.data) {
        setSession(result.data.access_token);
        updateUserInfo(result.data.user);
        navigate(appRoutes.products.fullPath);
      }
    } catch (e) {
      ErrorUtils.handleError<LoginFormProps>(e, {
        isUnprocessableEntityErrorFn: (error) => {
          const { email, password } = error.data.data ?? {};
          if (email) {
            form.setError('email', { message: email });
          }
          if (password) {
            form.setError('password', { message: password });
          }
        },
        isNotFoundErrorFn: () => {
          alert('API Not Found');
        },
      });
    }
  });

  return {
    form,
    onLogin,
  };
}
