import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Input } from 'shopeefrontend/components';
import { loginHandler } from 'shopeefrontend/features/authentication/login/login';
import { Link } from 'react-router-dom';
import appRoutes from 'shopeefrontend/routes/paths';
import { apiEndpoint } from 'shopeefrontend/utils/constants';

function LoginView() {
  const { t } = useTranslation();

  console.log(apiEndpoint);
  const {
    form: {
      formState: { errors },
      register,
    },
    onLogin,
  } = loginHandler();

  return (
    <div className='bg-white p-3 w-25 px-7 py-6 rounded'>
      <p className='text-xl'>{t('Đăng nhập')}</p>
      <form onSubmit={onLogin}>
        <Input
          type='email'
          className='mt-8'
          error={!!errors.email}
          helperText={errors.email?.message}
          placeholder={t('Email')}
          focusedOnMount
          {...register('email')}
        />
        <Input
          type='password'
          error={!!errors.password}
          helperText={errors.password?.message}
          placeholder={t('Password')}
          {...register('password')}
        />

        <Button type='submit' fullWidth>
          {t('Đăng nhập')}
        </Button>
      </form>
      <div className='flex justify-between items-center mt-2'>
        <p className='text-xs text-secondary-main'>{t('Quên mật khẩu')}</p>
        <p className='text-xs text-secondary-main'>{t('Đăng nhập với sms')}</p>
      </div>
      <hr className='my-4' />
      <div className='text-center'>
        <span className='text-gray-400 text-sm'>{t('Bạn mới biết đến Shopee?')}</span>{' '}
        <Link to={appRoutes.register.fullPath}>
          <span className='text-sm text-primary-main'>{t('Đăng ký')}</span>
        </Link>
      </div>
    </div>
  );
}

export default LoginView;
