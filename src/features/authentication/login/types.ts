import { defaultLoginFormValues } from 'shopeefrontend/features/authentication/login/constants';
import { User } from 'shopeefrontend/types/user.types';

export type LoginFormProps = typeof defaultLoginFormValues;

export interface LoginResponse {
  access_token: string;
  expires: string;
  user: User;
}
