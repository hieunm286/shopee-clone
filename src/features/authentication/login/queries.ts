import { useMutation } from '@tanstack/react-query';
import { authServices } from 'shopeefrontend/apis';

const login = () => useMutation(authServices.login);

export const loginQueries = {
  login,
};
