import Routes from 'shopeefrontend/routes';
import AuthProvider from 'shopeefrontend/components/AuthProvider';

function App() {
  return (
    <div className='App'>
      <AuthProvider>
        <Routes />
      </AuthProvider>
    </div>
  );
}

export default App;
