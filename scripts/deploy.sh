#!/bin/bash

PUBLIC_ADDRESS=$EC2_IPADDRESS

git pull
yarn install
yarn build
ls -al dist/
echo "Deploying to ${PUBLIC_ADDRESS}"
scp -r dist/ ubuntu@${PUBLIC_ADDRESS}:/home/ubuntu

echo "Finished copying the build files"
