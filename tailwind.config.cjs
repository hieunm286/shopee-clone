const plugin = require("tailwindcss/plugin")

/** @type {import('tailwindcss').Config} */

/** @type {number} */
const REM_PIXEL_RATIO = 16;
const pxToRem = (pxValue) => {
  return `${pxValue / REM_PIXEL_RATIO}rem`
}

module.exports = {
  jit: true,
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  corePlugins: {
    container: false
  },
  theme: {
    extend: {
      colors: {
        primary: {
          lighter: "#F05D40",
          main: "#ee4d2d",
        },
        secondary: {
          main: "#05a"
        }
      },
      height: {
        "10.5": pxToRem(42),
        21: pxToRem(84),
        150: pxToRem(600)
      },
      backgroundImage: {
        authenticationBanner: "url('https://cf.shopee.vn/file/sg-11134004-22120-um1uuybz9dlv43')"
      },
      maxWidth: {
        260: pxToRem(1040)
      },
      minHeight: {
        1.25: '1.25rem'
      },
      width: {
        25: pxToRem(400)
      }
    },
  },
  plugins: [
      plugin(function ({addComponents}) {
        addComponents({
          '.container': {
            maxWidth: pxToRem(1200)
          }
        })
      })
  ],
}
